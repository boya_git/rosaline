from db import session
from models import Subscriber


def is_user_exist(tg_id: int) -> bool:
    return session.query(Subscriber.id).filter_by(tg_id=tg_id).first() is not None


def add_sub(tg_id: int) -> int:
    sub = Subscriber(tg_id=tg_id, status=False)
    session.add(sub)
    session.commit()
    return tg_id


def sub_status_activate(tg_id: int) -> int:
    session.query(Subscriber).filter_by(tg_id=tg_id).update({'status': True})
    session.commit()
    return tg_id


def sub_status_deactivate(tg_id: int) -> int:
    session.query(Subscriber).filter_by(tg_id=tg_id).update({'status': False})
    session.commit()
    return tg_id


def get_active_subs():
    return session.query(Subscriber).filter_by(status=True).all()
