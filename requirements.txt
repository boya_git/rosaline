aiogram==2.18
aiohttp==3.8.1
aiosignal==1.2.0
appdirs==1.4.4
async-timeout==4.0.2
attrs==21.4.0
Babel==2.9.1
beautifulsoup4==4.10.0
certifi==2021.10.8
charset-normalizer==2.0.10
frozenlist==1.2.0
greenlet==1.1.2
idna==3.3
jedi==0.18.1
multidict==5.2.0
parso==0.8.3
prompt-toolkit==3.0.24
ptpython==3.0.20
Pygments==2.11.1
python-dotenv==0.19.2
pytz==2021.3
requests==2.27.1
soupsieve==2.3.1
SQLAlchemy==1.4.29
urllib3==1.26.7
wcwidth==0.2.5
yarl==1.7.2
