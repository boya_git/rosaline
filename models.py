from sqlalchemy import Column, Integer, String, Boolean
from db import Base


class WonderfulJourneyGuide(Base):
    __tablename__ = 'wonderful_journey_guides'

    id = Column(Integer, primary_key=True)
    article_id = Column(String(50), unique=True)
    article_link = Column(String(255), unique=True)
    map_link = Column(String(255))

    def __repr__(self) -> str:
        return f'<Article: {self.article_id}> | {self.article_link}'


class RedemptionCode(Base):
    __tablename__ = 'redemptions_codes'

    id = Column(Integer, primary_key=True)
    code = Column(String(50), unique=True)


class Subscriber(Base):
    __tablename__ = 'subscibers'

    id = Column(Integer, primary_key=True)
    tg_id = Column(Integer, unique=True)
    status = Column(Boolean)

    def __repr__(self) -> str:
        return f'<Telegram user id: {self.tg_id}>'
