import asyncio
from aiogram import Bot, Dispatcher, types, executor
from aiogram.types import MediaGroup

from config import Config
from db import engine
from models import Base
from parser import PAFKParser
from services import (is_user_exist,
                      add_sub, sub_status_activate,
                      sub_status_deactivate,
                      get_active_subs)


bot = Bot(token=Config.API_TOKEN)
dp = Dispatcher(bot)

# Create all tables if not exist
Base.metadata.create_all(engine)


@dp.message_handler(commands=['subscribe'])
async def subscribe(message: types.Message):
    tg_id = message.from_user.id
    if is_user_exist(tg_id):
        sub_status_activate(tg_id)
        await message.answer('Я буду рассказывать вам о последних обновлениях!')
    else:
        add_sub(tg_id)
        sub_status_activate(tg_id)
        await message.answer('Я буду рассказывать вам о последних обновлениях!'
                             'Рада знакомству :)')


@dp.message_handler(commands=['unsubscribe'])
async def unsubscribe(message: types.Message):
    tg_id = message.from_user.id
    if is_user_exist(tg_id):
        sub_status_deactivate(tg_id)
        await message.answer('Я не буду вас беспокоить!')
    else:
        add_sub(tg_id)
        await message.answer('Я бы в любом случае вас не потревожила,'
                             'ведь мы незнакомы!')


async def wjg_notificate():
    new_articles = PAFKParser.get_new()
    new_codes = PAFKParser.get_new_rcodes()

    if new_articles:
        subs = get_active_subs()
        for sub in subs:
            for article in new_articles:
                message = (f"Кажется, я тут кое-что нашла!\n"
                           f"Это карта с прохождением нового ЧП!\n"
                           f"Вот ссылка на статью с прохождением:\n"
                           f"{article.get('article_link')}")
                if article.get('map_link'):
                    media = MediaGroup()
                    media.attach_photo(photo=article.get('map_link'),
                                       caption=message)
                    await bot.send_media_group(chat_id=sub.tg_id,
                                               media=media,
                                               disable_notification=True)
                else:
                    await bot.send_message(chat_id=sub.tg_id,
                                           text=message,
                                           disable_notification=True)
    else:
        print('Nothing to do with WJG.')

    if new_codes:
        subs = get_active_subs()
        for sub in subs:
            for rcode in new_codes:
                message = (f"Похоже разработчики приготовили нам\n"
                           f"небольшой подарок!\n"
                           f"Это новый код возмещения:\n"
                           f"{rcode.get('code')}")
                await bot.send_message(chat_id=sub.tg_id,
                                       text=message,
                                       disable_notification=True)
    else:
        print('Nothing to do with RCODES.')


async def sheduled(interval):
    while True:
        await asyncio.sleep(interval)

        await wjg_notificate()
        # await bot.send_message(166132626, 'Проверка выполнена.', disable_notification=True)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(sheduled(3600))
    executor.start_polling(dp, skip_updates=True)
