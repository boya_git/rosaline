import requests
from bs4 import BeautifulSoup

from db import session
from models import WonderfulJourneyGuide, RedemptionCode


class PAFKParser():

    articles_url = 'https://playafkarena.ru/temnyj-les/chudesnoe-puteshestvie/'
    rc_url = 'https://playafkarena.ru/gajdy/kody-vozmeshheniya/'
    headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'ru,en;q=0.9,en-GB;q=0.8,en-US;q=0.7',
        'cache-control': 'max-age=0',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36 Edg/96.0.1054.62',
    }

    @classmethod
    def get_new(cls):
        articles = cls._wjg_parse()
        new = []
        for article in articles:
            if session.query(WonderfulJourneyGuide).filter_by(article_id=article.get('article_id')).first():
                return new
            else:
                cls._add_article(article)
                new.append(article)
        return new

    @classmethod
    def get_new_rcodes(cls):
        rcodes = cls._rcodes_parse()
        new = []
        for rcode in rcodes:
            if not session.query(RedemptionCode).filter_by(code=rcode.get('code')).first():
                cls._add_rcode(rcode)
                new.append(rcode)
        return new

    @staticmethod
    def _add_article(article: dict):
        session.add(WonderfulJourneyGuide(**article))
        session.commit()

    @staticmethod
    def _add_rcode(code: RedemptionCode):
        session.add(RedemptionCode(**code))
        session.commit()

    @classmethod
    def _wjg_parse(cls):
        """
        Wondefrul Journey Guide parser.
        """
        response = requests.get(cls.articles_url, headers=cls.headers)
        page = BeautifulSoup(response.text, features='html.parser')
        row_articles = page.find_all('article')
        articles = []
        for item in row_articles:
            article = {'article_id': item['id'],
                       'article_link': item.find('a', {'rel': False})['href'],
                       'map_link': cls._get_guide_map(item.find('a', {'rel': False})['href']),}
            articles.append(article)
        return articles

    @classmethod
    def _rcodes_parse(cls):
        """
        Parse redemption codes
        """
        response = requests.get(cls.rc_url, headers=cls.headers)
        page = BeautifulSoup(response.text, features='html.parser')
        elemenets = page.select('article > div.entry-content > div > div > p')
        rcodes = [{'code': rcode.getText()} for rcode in elemenets]
        return rcodes

    @classmethod
    def _get_guide_map(cls, url: str):
        """
        Concrete Guide page parser.
        """
        response = requests.get(url, headers=cls.headers)
        page = BeautifulSoup(response.text, features='html.parser')
        figures = page.find_all('figure')
        for figure in figures:
            if figure.find('a'):
                return figure.find('a').find('img')['src']
        return None
