import os
from dotenv import load_dotenv


BASEDIR = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(BASEDIR, '.env'))


class Config:
    API_TOKEN = os.environ.get('API_TOKEN')
    DB_NAME = os.environ.get('DB_NAME')