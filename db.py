from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

from config import Config


engine = create_engine('sqlite:///' + Config.DB_NAME + '.db', echo=True)
session = sessionmaker(bind=engine)()
Base = declarative_base()
